﻿using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Naruto.Redis;
using Naruto.Redis.Interface;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Naruto.Domain.Model.Entities;
using System.Linq;
using System.IO;
using System.Threading;
using Naruto.Redis.Config;

namespace Naruto.XUnitTest
{
    public class RedisTest
    {
        IServiceCollection services = new ServiceCollection();

        private readonly IRedisRepository redis;
        public RedisTest()
        {
            services.AddLogging();
            //注入redis仓储服务
            services.AddRedisRepository(options =>
            {
                options.Connection = new string[] {
                };
                options.DefaultDataBase = 0;
                options.RedisPrefix = new RedisPrefixKey();
            });
            redis = services.BuildServiceProvider().GetService<IRedisRepository>();
        }

        /// <summary>
        /// 定义一个库存值
        /// </summary>
        internal int stock = 10;

        [Fact]
        public async Task Lock()
        {
            {
                await using (var redlock = await redis.Lock.LockAsync("testLock", TimeSpan.FromSeconds(20)))
                {
                    if (redlock.IsAcquired)
                    {
                       await Task.Delay(TimeSpan.FromSeconds(15));
                    }
                }
            }


        }

        //[Fact]
        //public async Task LockWait()
        //{
        //    await redis.Lock.LockAsync("ceshi", TimeSpan.FromSeconds(180));

        //    await redis.Lock("ceshi", TimeSpan.FromSeconds(180), TimeSpan.FromMilliseconds(300));
        //}


        [Fact]
        public void test()
        {
            var redis = ConnectionMultiplexer.Connect("127.0.0.1");
            var res = redis.GetDatabase().StringIncrement("test", 0d);
            for (int i = 0; i < 10; i++)
            {
                res = redis.GetDatabase().StringIncrement("test");
            }
            var rr = redis.GetDatabase().StringGet("test");

            res = redis.GetDatabase().StringDecrement("test");
            for (int i = 0; i < 5; i++)
            {
                res = redis.GetDatabase().StringDecrement("test");
            }

            var redisbase = services.BuildServiceProvider().GetService<IRedisRepository>();
            res = redisbase.String.Increment("test");
            for (int i = 0; i < 10; i++)
            {
                res = redisbase.String.Increment("test");
            }
            for (int i = 0; i < 10; i++)
            {
                res = redisbase.String.Decrement("test");
            }
            Console.WriteLine("1");
        }

        [Fact]
        public async Task Test_Stream()
        {
            ConfigurationOptions configurationOptions = new ConfigurationOptions();
            configurationOptions.EndPoints.Add("");
            configurationOptions.EndPoints.Add("");
            configurationOptions.EndPoints.Add("");
            var redis = ConnectionMultiplexer.Connect(configurationOptions);
            //新增流数据
            var ress = await redis.GetDatabase(0).StreamAddAsync(nameof(Test_Stream2), new Dictionary<string, string> {
                    {"head","asdasd" },
                     {"body","body" }
                }.Select(a => new NameValueEntry(a.Key, a.Value)).ToArray());
            //新增流数据
            var res = await redis.GetDatabase(0).StreamAddAsync("stream", new NameValueEntry[] {
                new NameValueEntry("heard",Encoding.UTF8.GetBytes("asdasd")),
                new NameValueEntry("body","asdasd2"),
            });
            var type = await redis.GetDatabase().KeyTypeAsync("stream");


            var groupInfo = await redis.GetDatabase().StreamGroupInfoAsync("stream");

            if (!groupInfo.Any(a => a.Name == "testGroupName"))
            {
                var addGroup = await redis.GetDatabase().StreamCreateConsumerGroupAsync("stream", "testGroupName");
            }
            var result = await redis.GetDatabase().StreamReadGroupAsync(new StreamPosition[] {
                new StreamPosition("stream",StreamPosition.NewMessages)
            }, "testGroupName", "testGroupName", 10);
            foreach (var item in result)
            {
                foreach (var item2 in item.Entries)
                {
                    var sssssa = Encoding.UTF8.GetString(item2["body"]);
                }
            }
            groupInfo = await redis.GetDatabase().StreamGroupInfoAsync("stream");

            ////删除流数据
            var ss = await redis.GetDatabase().StreamDeleteAsync("stream", new RedisValue[] { "123" });
            //消息确认
            var ackInfo = await redis.GetDatabase().StreamAcknowledgeAsync("stream", "groupname", res.ToString());
        }

        [Fact]
        public async Task Test_Stream2()
        {
            var stream = redis.Stream;
            for (int i = 0; i < 10; i++)
            {
                var ss = stream.Add(nameof(Test_Stream2), new Dictionary<string, byte[]> {
                    {"head",Encoding.UTF8.GetBytes( "asdasd"+i) },
                     {"body",Encoding.UTF8.GetBytes("body"+i) }
                });
                var sfra = await stream.AddAsync("asdasd", new Dictionary<string, byte[]> {
                    {"head",Encoding.UTF8.GetBytes("asdasd"+i) },
                     {"body",Encoding.UTF8.GetBytes("asdasdbody"+i) }
                });
            }

            var groupIngfo = await stream.GetConsumerGroupInfoAsync(nameof(Test_Stream2));
            await stream.CreateConsumerGroupAsync(nameof(Test_Stream2), "Test_Stream2_Group");
            await stream.CreateConsumerGroupAsync("asdasd", "Test_Stream2_Group");

            var streamEntity = await stream.StreamReadGroupAsync(new StreamPosition[] {
                new StreamPosition(nameof(Test_Stream2),StreamPosition.NewMessages),
                 new StreamPosition("asdasd",StreamPosition.NewMessages)
            }, "Test_Stream2_Group", "Test_Stream2_Group", 15);
            foreach (var item in streamEntity)
            {
                foreach (var item2 in item.Entries)
                {
                    var ss = item2.ToString();
                    var sss = Encoding.UTF8.GetString(item2["body"]).ToString();
                    var resack = await stream.AckAsync(item.Key, "Test_Stream2_Group", item2.Id);
                }
            }
        }
        //[Fact]
        //public async Task Publish()
        //{
        //    var path = Path.GetTempPath();
        //    var redis = ConnectionMultiplexer.Connect("127.0.0.1");
        //    ISubscriber subscriber = redis.GetSubscriber();
        //    Parallel.For(0, 1000, async item =>
        //    {
        //        //发布
        //        await subscriber.PublishAsync("push", item.ToString);
        //    });

        //}
        [Fact]
        public async Task RedisTest1()
        {

            ConcurrentQueue<setting> settings1 = new ConcurrentQueue<setting>();

            Parallel.For(0, 1, (item) =>
            {
                settings1.Enqueue(new setting() { Contact = "1", Description = "1", DuringTime = "1", Integral = 1, Rule = "1" });
            });

            await redis.List.AddAsync<setting>(1, "test", settings1.ToList());
        }
        [Fact]
        public async Task RedisTest2()
        {
            //await redis.SortedSet.AddAsync<string>("testSortedSet", "1", 111);
            //await redis.SortedSet.RemoveAsync("testSortedSet", "1");
            //redis.SortedSet.GetAsync<>();
            //for (int i = 0; i < 100; i++)
            //{
            //    await redis.SortedSet.AddAsync<test1>("testSortedSet1", new test1
            //    {
            //        s = $"{i}"
            //    }, i);
            //}
            var res = await redis.SortedSet.GetListAsync<test1>("testSortedSet1", 10);
            var l = await redis.SortedSet.RemoveByScoreAsync("testSortedSet1", 0, 10);
            redis.SortedSet.RemoveByScore("testSortedSet1", 31, 54);

            Parallel.For(1, 10000, async (item) =>
             {
                 await redis.String.IncrementAsync("1");
             });
        }
        [Fact]
        public async Task Pub()
        {

            await redis.Subscribe.SubscribeAsync("test", (msg, value) =>
             {
                 Console.WriteLine(value);
             });
        }

        [Fact]
        public async Task StringTest()
        {
            for (int i = 0; i < 5; i++)
            {
                using (var servicesscope = services.BuildServiceProvider().CreateScope())
                {
                    var redis = servicesscope.ServiceProvider.GetRequiredService<IRedisRepository>();
                    await redis.String.AddAsync(1, "1", "1");
                    await redis.String.GetAsync(1, "1");
                }
            }


        }
        [Fact]
        public void Remove()
        {
            redis.Key.Remove(new List<string>() { "test2", "zhang" });
        }
    }

    public class test1
    {
        public string s { get; set; }
    }
}
