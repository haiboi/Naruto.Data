﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Naruto.Redis.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public class AssemblyExtension
    {
        /// <summary>
        /// 加载嵌入的资源
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string LoadManifestResource(string name)
        {
            using var resourceStream = typeof(AssemblyExtension).Assembly.GetManifestResourceStream(name);
            using StreamReader stringReader = new StreamReader(resourceStream);
            return stringReader.ReadToEnd();
        }
    }
}
