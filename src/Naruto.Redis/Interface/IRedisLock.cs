﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Naruto.Redis.Interface
{
    /// <summary>
    /// 张海波
    /// 2019-12-6
    /// 锁
    /// </summary>
    public interface IRedisLock : IRedisDependency
    {

        /// <summary>
        /// 锁
        /// </summary>
        /// <param name="key">key </param>
        /// <param name="expiry">过期时间</param>
        /// <param name="waitTime">等待时间</param>
        /// <param name="delayTime">轮询的间隔时间</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<IRedLock> LockAsync(string key, TimeSpan expiry, TimeSpan waitTime, TimeSpan delayTime, CancellationToken cancellationToken = default);

        /// <summary>
        /// 锁 默认会重试三次获取锁
        /// </summary>
        /// <param name="key">key </param>
        /// <param name="expiry">过期时间</param>
        /// <returns></returns>
        Task<IRedLock> LockAsync(string key, TimeSpan expiry, CancellationToken cancellationToken = default);
    }
}
