﻿using Naruto.Redis.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Redis.Interface
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRedLock:IAsyncDisposable
    {
        /// <summary>
        /// 资源名称
        /// </summary>
        public string ResourceName { get; }
        /// <summary>
        /// 锁id
        /// </summary>
        public string LockId { get; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public TimeSpan ExpireTime { get; }

        /// <summary>
        /// 是否成功 获取到锁
        /// </summary>
        public bool IsAcquired { get; }
        /// <summary>
        /// 锁的状态
        /// </summary>
        public RedLockStatus Status { get; }
    }
}
