﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.Redis.Interface
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRedLockConnectionFactory : IRedisDependency, IDisposable
    {
        /// <summary>
        /// 创建连接
        /// </summary>
        /// <returns></returns>
        Task<List<ConnectionMultiplexer>> CreateConnectionAsync();
    }
}
