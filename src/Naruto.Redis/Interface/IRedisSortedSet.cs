﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.Redis.Interface
{
    /// <summary>
    /// 张海波
    /// 2019-12-6
    /// SortedSet操作
    /// </summary>
    public interface IRedisSortedSet : IRedisDependency
    {

        #region 同步

        /// <summary>
        /// SortedSet 新增
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="member"></param>
        /// <param name="score"></param>
        /// <returns></returns>
        bool Add<T>(string key, T value, double score);

        /// <summary>
        /// 获取SortedSet的数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存key</param>
        /// <param name="maxScore">排序的最大值</param>
        /// <param name="minScore">排序的最小值</param>
        /// <returns></returns>
        List<T> GetList<T>(string key, double minScore = double.NegativeInfinity, double maxScore = double.PositiveInfinity, Order order = Order.Ascending, long skip = 0, long take = -1);

        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        long Length(string key);

        /// <summary>
        /// 移除SortedSet
        /// </summary>
        bool Remove<T>(string key, T value);
        /// <summary>
        /// 根据排序的值删除数据
        /// </summary>
        /// <param name="minScore">最小的排序值</param>
        /// <param name="maxScore">最大的排序值</param>
        /// <returns></returns>
        long RemoveByScore(string key, double minScore, double maxScore);
        #endregion

        #region 异步

        /// <summary>
        /// SortedSet 新增
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="member"></param>
        /// <param name="score"></param>
        /// <returns></returns>
        Task<bool> AddAsync<T>(string key, T value, double score);

        /// <summary>
        /// 获取SortedSet的数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存key</param>
        /// <param name="maxScore">排序的最大值</param>
        /// <param name="minScore">排序的最小值</param>
        /// <returns></returns>
        Task<List<T>> GetListAsync<T>(string key, double minScore = double.NegativeInfinity, double maxScore = double.PositiveInfinity, Order order = Order.Ascending, long skip = 0, long take = -1) where T : new();
        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<long> LengthAsync(string key);

        /// <summary>
        /// 移除SortedSet
        /// </summary>
        Task<bool> RemoveAsync<T>(string key, T value);
        /// <summary>
        /// 根据排序的值删除数据
        /// </summary>
        /// <param name="minScore">最小的排序值</param>
        /// <param name="maxScore">最大的排序值</param>
        /// <returns></returns>
        Task<long> RemoveByScoreAsync(string key, double minScore, double maxScore);
        #endregion

        #region database

        #region 同步

        /// <summary>
        /// SortedSet 新增
        /// </summary>
        /// <param name="key">缓存key</param>
        /// <param name="value">存储的值</param>
        /// <param name="score">排序的值</param>
        /// <returns></returns>
        bool Add<T>(int dataBase, string key, T value, double score);

        /// <summary>
        /// 获取SortedSet的数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存key</param>
        /// <param name="maxScore">排序的最大值</param>
        /// <param name="minScore">排序的最小值</param>
        /// <returns></returns>
        List<T> GetList<T>(int dataBase, string key, double minScore = double.NegativeInfinity, double maxScore = double.PositiveInfinity, Order order = Order.Ascending, long skip = 0, long take = -1);

        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        long Length(int dataBase, string key);

        /// <summary>
        /// 移除SortedSet
        /// </summary>
        bool Remove<T>(int dataBase, string key, T value);
        /// <summary>
        /// 根据排序的值删除数据
        /// </summary>
        /// <param name="minScore">最小的排序值</param>
        /// <param name="maxScore">最大的排序值</param>
        /// <returns></returns>
        long RemoveByScore(int dataBase, string key, double minScore, double maxScore);
        #endregion

        #region 异步

        /// <summary>
        /// SortedSet 新增
        /// </summary>
        /// <param name="key">缓存key</param>
        /// <param name="value">存储的值</param>
        /// <param name="score">排序值</param>
        /// <returns></returns>
        Task<bool> AddAsync<T>(int dataBase, string key, T value, double score);

        /// <summary>
        /// 获取SortedSet的数据
        /// </summary>
        /// <param name="key">缓存key</param>
        /// <param name="maxScore">排序的最大值</param>
        /// <param name="minScore">排序的最小值</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        Task<List<T>> GetListAsync<T>(int dataBase, string key, double minScore = double.NegativeInfinity, double maxScore = double.PositiveInfinity, Order order = Order.Ascending, long skip = 0, long take = -1) where T : new();
        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<long> LengthAsync(int dataBase, string key);

        /// <summary>
        /// 移除SortedSet
        /// </summary>
        Task<bool> RemoveAsync<T>(int dataBase, string key, T value);
        /// <summary>
        /// 根据排序的值删除数据
        /// </summary>
        /// <param name="minScore">最小的排序值</param>
        /// <param name="maxScore">最大的排序值</param>
        /// <returns></returns>
        Task<long> RemoveByScoreAsync(int dataBase, string key, double minScore, double maxScore);
        #endregion

        #endregion
    }
}
