﻿using Naruto.Redis.Interface;
using Naruto.Redis.Config;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naruto.Redis.Connection;

namespace Naruto.Redis.Internal
{
    public class RedisString : IRedisString
    {

        private readonly IRedisConnection  redisConnection;

        private readonly RedisPrefixKey redisPrefixKey;

        /// <summary>
        /// 实例化连接
        /// </summary>
        public RedisString(IRedisConnection _redisConnection, IOptions<RedisOptions> options)
        {
            redisConnection = _redisConnection;
            //初始化key的前缀
            redisPrefixKey = options.Value.RedisPrefix ?? new RedisPrefixKey();
        }

        #region 同步

        /// <summary>
        /// 保存字符串
        /// 当key不存在的时候
        /// </summary>
        public bool AddNotExists(string key, string value, TimeSpan? expiry = default) => AddNotExists(redisConnection.DataBase, key, value, expiry);
        /// <summary>
        /// 保存字符串
        /// </summary>
        public bool Add(string key, string value, TimeSpan? expiry = default) => Add(redisConnection.DataBase, key, value, expiry);
        /// <summary>
        /// 保存对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public bool Add<T>(string key, T value, TimeSpan? expiry = default) => Add<T>(redisConnection.DataBase, key, value, expiry);

        /// <summary>
        /// 保存集合对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public bool Add<T>(string key, List<T> value, TimeSpan? expiry = default) => Add<T>(redisConnection.DataBase, key, value, expiry);


        /// <summary>
        /// 获取字符串
        /// </summary>
        public string Get(string key) => Get(redisConnection.DataBase, key);

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public T Get<T>(string key) => Get<T>(redisConnection.DataBase, key);


        /// <summary>
        /// 自增
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public long Increment(string key, long value = 1) => Increment(redisConnection.DataBase, key, value);

        /// <summary>
        /// 递减
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long Decrement(string key, long value = 1) => Decrement(redisConnection.DataBase, key, value);
        #endregion

        #region 异步
        /// <summary>
        /// 保存字符串
        /// 当key不存在的时候
        /// </summary>
        public async Task<bool> AddNotExistsAsync(string key, string value, TimeSpan? expiry = default) => await AddNotExistsAsync(redisConnection.DataBase,key, value, expiry);
        /// <summary>
        /// 保存字符串
        /// </summary>
        public async Task<bool> AddAsync(string key, string value, TimeSpan? expiry = default) => await AddAsync(redisConnection.DataBase, key, value, expiry);
        /// <summary>
        /// 保存对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public async Task<bool> AddAsync<T>(string key, T value, TimeSpan? expiry = default) => await AddAsync<T>(redisConnection.DataBase, key, value, expiry);

        /// <summary>
        /// 保存集合对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public async Task<bool> AddAsync<T>(string key, List<T> value, TimeSpan? expiry = default) => await AddAsync<T>(redisConnection.DataBase, key, value, expiry);

        /// <summary>
        /// 获取字符串
        /// </summary>
        public async Task<string> GetAsync(string key) => await GetAsync(redisConnection.DataBase, key);

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public async Task<T> GetAsync<T>(string key) => await GetAsync<T>(redisConnection.DataBase, key);

        /// <summary>
        /// 自增
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public async Task<long> IncrementAsync(string key, long value = 1) => await IncrementAsync(redisConnection.DataBase, key, value);

        /// <summary>
        /// 递减
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<long> DecrementAsync(string key, long value = 1) => await DecrementAsync(redisConnection.DataBase, key, value);
        #endregion

        #region database

        #region 同步
        /// <summary>
        /// 保存字符串
        /// </summary>
        public bool Add(int dataBase, string key, string value, TimeSpan? expiry = default)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(nameof(value));
            }
            return redisConnection.Data(dataBase).StringSet(redisPrefixKey.StringPrefixKey + key, value, expiry);
        }

        /// <summary>
        /// 保存字符串
        /// 当key不存在的时候
        /// </summary>
        public bool AddNotExists(int dataBase, string key, string value, TimeSpan? expiry = default)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(nameof(value));
            }
            return redisConnection.Data(dataBase).StringSet(redisPrefixKey.StringPrefixKey + key, value, expiry, When.NotExists);
        }
        /// <summary>
        /// 保存对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public bool Add<T>(int dataBase, string key, T value, TimeSpan? expiry = default)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            key = redisPrefixKey.StringPrefixKey + key;
            var res = RedisBase.ConvertJson(value);
            return redisConnection.Data(dataBase).StringSet(key, res, expiry);
        }

        /// <summary>
        /// 保存集合对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public bool Add<T>(int dataBase, string key, List<T> value, TimeSpan? expiry = default)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null || value.Count() <= 0)
            {
                throw new ArgumentNullException(nameof(value));
            }
            key = redisPrefixKey.StringPrefixKey + key;
            List<T> li = new List<T>();
            foreach (var item in value)
            {
                li.Add(item);
            }
            var res = RedisBase.ConvertJson(li);
            return redisConnection.Data(dataBase).StringSet(key, res, expiry);
        }

        /// <summary>
        /// 获取字符串
        /// </summary>
        public string Get(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return redisConnection.Data(dataBase).StringGet(redisPrefixKey.StringPrefixKey + key);
        }

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public T Get<T>(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return RedisBase.ConvertObj<T>(redisConnection.Data(dataBase).StringGet(redisPrefixKey.StringPrefixKey + key));
        }


        /// <summary>
        /// 自增
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public long Increment(int dataBase, string key, long value = 1)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            key = redisPrefixKey.StringPrefixKey + key;
            return redisConnection.Data(dataBase).StringIncrement(key, value);
        }

        /// <summary>
        /// 递减
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long Decrement(int dataBase, string key, long value = 1)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            key = redisPrefixKey.StringPrefixKey + key;
            return redisConnection.Data(dataBase).StringDecrement(key, value);
        }
        #endregion

        #region 异步

        /// <summary>
        /// 保存字符串
        /// 当key不存在的时候
        /// </summary>
        public async Task<bool> AddNotExistsAsync(int dataBase, string key, string value, TimeSpan? expiry = default)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(nameof(value));
            }
            return await redisConnection.Data(dataBase).StringSetAsync(redisPrefixKey.StringPrefixKey + key, value, expiry, When.NotExists);
        }

        /// <summary>
        /// 保存字符串
        /// </summary>
        public async Task<bool> AddAsync(int dataBase, string key, string value, TimeSpan? expiry = default)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(nameof(value));
            }
            return await redisConnection.Data(dataBase).StringSetAsync(redisPrefixKey.StringPrefixKey + key, value, expiry).ConfigureAwait(false);
        }
        /// <summary>
        /// 保存对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public async Task<bool> AddAsync<T>(int dataBase, string key, T value, TimeSpan? expiry = default)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            return await redisConnection.Data(dataBase).StringSetAsync(redisPrefixKey.StringPrefixKey + key, RedisBase.ConvertJson(value), expiry).ConfigureAwait(false);
        }

        /// <summary>
        /// 保存集合对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public async Task<bool> AddAsync<T>(int dataBase, string key, List<T> value, TimeSpan? expiry = default)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null || value.Count() <= 0)
            {
                throw new ArgumentNullException(nameof(value));
            }
            key = redisPrefixKey.StringPrefixKey + key;
            List<T> li = new List<T>();
            foreach (var item in value)
            {
                li.Add(item);
            }
            return await redisConnection.Data(dataBase).StringSetAsync(key, RedisBase.ConvertJson(li), expiry).ConfigureAwait(false);
        }

        /// <summary>
        /// 获取字符串
        /// </summary>
        public async Task<string> GetAsync(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return await redisConnection.Data(dataBase).StringGetAsync(redisPrefixKey.StringPrefixKey + key).ConfigureAwait(false);
        }

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public async Task<T> GetAsync<T>(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            key = redisPrefixKey.StringPrefixKey + key;
            var value = await redisConnection.Data(dataBase).StringGetAsync(key).ConfigureAwait(false);
            if (value.ToString() == null)
            {
                return default(T);
            }
            return RedisBase.ConvertObj<T>(value);
        }

        /// <summary>
        /// 自增
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public async Task<long> IncrementAsync(int dataBase, string key, long value = 1)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            key = redisPrefixKey.StringPrefixKey + key;
            return await redisConnection.Data(dataBase).StringIncrementAsync(key, value);
        }

        /// <summary>
        /// 递减
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<long> DecrementAsync(int dataBase, string key, long value = 1)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            key = redisPrefixKey.StringPrefixKey + key;
            return await redisConnection.Data(dataBase).StringDecrementAsync(key, value);
        }
        #endregion

        #endregion
    }
}
