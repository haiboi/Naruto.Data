﻿using Naruto.Redis.Extensions;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Naruto.Redis.Internal
{
    /// <summary>
    /// lua脚本资源
    /// </summary>
    internal class LuaResources
    {

        /// <summary>
        /// 解锁
        /// </summary>
        public static string UnLockScript = AssemblyExtension.LoadManifestResource("Naruto.Redis.Lua.UnLock.lua");

        /// <summary>
        /// 扩展时间
        /// </summary>
        public static string ExtendedTimeScript = AssemblyExtension.LoadManifestResource("Naruto.Redis.Lua.ExtendedLockTime.lua");
    }
}
