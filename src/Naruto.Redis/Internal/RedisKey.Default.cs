﻿using Naruto.Redis.Interface;
using Naruto.Redis.Config;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naruto.Redis.Connection;

namespace Naruto.Redis.Internal
{
    public class RedisKey : IRedisKey
    {
        private readonly IRedisConnection redisConnection;

        private readonly RedisPrefixKey redisPrefixKey;

        /// <summary>
        /// 实例化连接
        /// </summary>
        public RedisKey(IRedisConnection _redisConnection, IOptions<RedisOptions> options)
        {
            redisConnection = _redisConnection;
            //初始化key的前缀
            redisPrefixKey = options.Value.RedisPrefix ?? new RedisPrefixKey();
        }
        #region 同步
        /// <summary>
        /// 移除key
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key, KeyOperatorEnum keyOperatorEnum = default) => Remove(redisConnection.DataBase, key, keyOperatorEnum);

        /// <summary>
        /// 移除key
        /// </summary>
        /// <param name="key"></param>
        public void Remove(List<string> key, KeyOperatorEnum keyOperatorEnum = default) => Remove(redisConnection.DataBase, key, keyOperatorEnum);
        /// <summary>
        /// 判断key是否存在
        /// </summary>
        /// <param name="key"></param>
        public bool Exists(string key, KeyOperatorEnum keyOperatorEnum = default) => Exists(redisConnection.DataBase, key, keyOperatorEnum);
        #endregion

        #region 异步
        /// <summary>
        /// 移除key
        /// </summary>
        /// <param name="key"></param>
        public async Task<bool> RemoveAsync(string key, KeyOperatorEnum keyOperatorEnum = default) => await RemoveAsync(redisConnection.DataBase, key, keyOperatorEnum);
        /// <summary>
        /// 移除key
        /// </summary>
        /// <param name="key"></param>
        public async Task<long> RemoveAsync(List<string> key, KeyOperatorEnum keyOperatorEnum = default) => await RemoveAsync(redisConnection.DataBase, key, keyOperatorEnum);
        /// <summary>
        /// 判断key是否存在
        /// </summary>
        /// <param name="key"></param>
        public async Task<bool> ExistsAsync(string key, KeyOperatorEnum keyOperatorEnum = default) => await ExistsAsync(redisConnection.DataBase, key, keyOperatorEnum);
        #endregion

        #region database

        #region 同步
        /// <summary>
        /// 移除key
        /// </summary>
        /// <param name="key"></param>
        public void Remove(int dataBase, string key, KeyOperatorEnum keyOperatorEnum = default)
        {
            if (keyOperatorEnum == KeyOperatorEnum.String)
                key = redisPrefixKey.StringPrefixKey + key;
            else if (keyOperatorEnum == KeyOperatorEnum.List)
                key = redisPrefixKey.ListPrefixKey + key;
            else if (keyOperatorEnum == KeyOperatorEnum.Set)
                key = redisPrefixKey.SetPrefixKey + key;
            else if (keyOperatorEnum == KeyOperatorEnum.Hash)
                key = redisPrefixKey.HashPrefixKey + key;
            else if (keyOperatorEnum == KeyOperatorEnum.SortedSet)
                key = redisPrefixKey.SortedSetKey + key;
            redisConnection.Data(dataBase).KeyDelete(key);
        }

        /// <summary>
        /// 移除key
        /// </summary>
        /// <param name="key"></param>
        public void Remove(int dataBase, List<string> key, KeyOperatorEnum keyOperatorEnum = default)
        {
            if (key == null || key.Count() <= 0)
            {
                throw new ArgumentNullException(nameof(key));
            }
            List<string> removeList = new List<string>();
            key.ForEach(item =>
            {
                item = GetKey(item, keyOperatorEnum);
                removeList.Add(item);
            });
            redisConnection.Data(dataBase).KeyDelete(RedisBase.ConvertRedisKeys(removeList));
        }
        /// <summary>
        /// 判断key是否存在
        /// </summary>
        /// <param name="key"></param>
        public bool Exists(int dataBase, string key, KeyOperatorEnum keyOperatorEnum = default)
        {
            key = GetKey(key, keyOperatorEnum);
            return redisConnection.Data(dataBase).KeyExists(key);
        }
        #endregion

        #region 异步
        /// <summary>
        /// 移除key
        /// </summary>
        /// <param name="key"></param>
        public async Task<bool> RemoveAsync(int dataBase, string key, KeyOperatorEnum keyOperatorEnum = default)
        {
            key = GetKey(key, keyOperatorEnum);
            return await redisConnection.Data(dataBase).KeyDeleteAsync(key).ConfigureAwait(false);
        }
        /// <summary>
        /// 移除key
        /// </summary>
        /// <param name="key"></param>
        public async Task<long> RemoveAsync(int dataBase, List<string> key, KeyOperatorEnum keyOperatorEnum = default)
        {
            if (key == null || key.Count() <= 0)
            {
                throw new ArgumentNullException(nameof(key));
            }
            List<string> removeList = new List<string>();

            key.ForEach(item =>
            {
                item = GetKey(item, keyOperatorEnum);
                removeList.Add(item);
            });
            return await redisConnection.Data(dataBase).KeyDeleteAsync(RedisBase.ConvertRedisKeys(removeList)).ConfigureAwait(false);
        }
        /// <summary>
        /// 判断key是否存在
        /// </summary>
        /// <param name="key"></param>
        public async Task<bool> ExistsAsync(int dataBase, string key, KeyOperatorEnum keyOperatorEnum = default)
        {
            key = GetKey(key, keyOperatorEnum);
            return await redisConnection.Data(dataBase).KeyExistsAsync(key).ConfigureAwait(false);
        }
        #endregion

        #endregion

        /// <summary>
        /// 获取key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="keyOperatorEnum"></param>
        /// <returns></returns>
        private string GetKey(string key, KeyOperatorEnum keyOperatorEnum = default)
        {
            if (keyOperatorEnum == KeyOperatorEnum.String)
            {
                key = redisPrefixKey.StringPrefixKey + key;
            }
            else if (keyOperatorEnum == KeyOperatorEnum.List)
            {
                key = redisPrefixKey.ListPrefixKey + key;
            }
            else if (keyOperatorEnum == KeyOperatorEnum.Set)
            {
                key = redisPrefixKey.SetPrefixKey + key;
            }
            else if (keyOperatorEnum == KeyOperatorEnum.Hash)
            {
                key = redisPrefixKey.HashPrefixKey + key;
            }
            else if (keyOperatorEnum == KeyOperatorEnum.SortedSet)
            {
                key = redisPrefixKey.SortedSetKey + key;
            }
            return key;
        }

    }
}
