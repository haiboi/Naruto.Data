﻿using Naruto.Redis.Interface;
using Naruto.Redis.Config;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naruto.Redis.Connection;

namespace Naruto.Redis.Internal
{
    public class RedisHash : IRedisHash
    {
        private readonly IRedisConnection  redisConnection;

        private readonly RedisPrefixKey redisPrefixKey;

        /// <summary>
        /// 实例化连接
        /// </summary>
        public RedisHash(IRedisConnection _redisConnection, IOptions<RedisOptions> options)
        {
            redisConnection = _redisConnection;
            //初始化key的前缀
            redisPrefixKey = options.Value.RedisPrefix ?? new RedisPrefixKey();
        }
        #region 同步

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="key">主键</param>
        /// <param name="hashField"需要删除的字段</param>
        /// <returns></returns>
        public bool Delete(string key, string hashField)
        {
            return Delete(redisConnection.DataBase, key, hashField);
        }

        /// <summary>
        /// 删除多条
        /// </summary>
        /// <param name="key">主键</param>
        /// <param name="hashFields"需要删除的字段</param>
        /// <returns></returns>
        public long HashDelete(string key, string[] hashFields)
        {
            return HashDelete(redisConnection.DataBase, key, hashFields);
        }

        /// <summary>
        /// 验证是否存在指定列
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public bool Exists(string key, string hashField) => Exists(redisConnection.DataBase, key, hashField);
        /// <summary>
        /// 获取指定的列的值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public string Get(string key, string hashField) => Get(redisConnection.DataBase, key, hashField);
        /// <summary>
        /// 获取所有的数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetAll(string key) => GetAll(redisConnection.DataBase, key);
        /// <summary>
        /// 获取多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashFields"></param>
        /// <returns></returns>
        public string[] Get(string key, string[] hashFields) => Get(redisConnection.DataBase, key, hashFields);
        /// <summary>
        /// 获取hash的长度
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long Length(string key) => Length(redisConnection.DataBase, key);

        /// <summary>
        /// 存储hash值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashFields">存储的数据key-value结构</param>
        /// <returns></returns>
        public void Add(string key, HashEntry[] hashFields) => Add(redisConnection.DataBase, key, hashFields);

        /// <summary>
        /// 储存单条hash值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField">字段名</param>
        /// <param name="value">值</param>
        /// <returns></returns>
        public bool Add(string key, string hashField, string value) => Add(redisConnection.DataBase, key, hashField, value);
        /// <summary>
        /// 返回所有值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string[] HashValues(string key) => HashValues(redisConnection.DataBase, key);
        #endregion

        #region 异步

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="key">主键</param>
        /// <param name="hashField"需要删除的字段</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(string key, string hashField) => await DeleteAsync(redisConnection.DataBase, key, hashField);

        /// <summary>
        /// 删除多条
        /// </summary>
        /// <param name="key">主键</param>
        /// <param name="hashFields"需要删除的字段</param>
        /// <returns></returns>
        public async Task<long> DeleteAsync(string key, string[] hashFields) => await DeleteAsync(redisConnection.DataBase, key, hashFields);

        /// <summary>
        /// 验证是否存在指定列
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public async Task<bool> ExistsAsync(string key, string hashField) => await ExistsAsync(redisConnection.DataBase, key, hashField);
        /// <summary>
        /// 获取指定的列的值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public async Task<string> GetAsync(string key, string hashField) => await GetAsync(redisConnection.DataBase, key, hashField);

        /// <summary>
        /// 获取所有的数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, string>> GetAllAsync(string key) => await GetAllAsync(redisConnection.DataBase, key);
        /// <summary>
        /// 获取多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashFields"></param>
        /// <returns></returns>
        public async Task<string[]> GetAsync(string key, string[] hashFields) => await GetAsync(redisConnection.DataBase, key, hashFields);
        /// <summary>
        /// 获取hash的长度
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<long> LengthAsync(string key) => await LengthAsync(redisConnection.DataBase, key);

        /// <summary>
        /// 存储hash值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashFields">存储的数据key-value结构</param>
        /// <returns></returns>
        public async Task AddAsync(string key, HashEntry[] hashFields) => await AddAsync(redisConnection.DataBase, key, hashFields);

        /// <summary>
        /// 储存单条hash值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField">字段名</param>
        /// <param name="value">值</param>
        /// <returns></returns>
        public async Task<bool> AddAsync(string key, string hashField, string value) => await AddAsync(redisConnection.DataBase, key, hashField, value);

        /// <summary>
        /// 返回所有值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<string[]> ValuesAsync(string key) => await ValuesAsync(redisConnection.DataBase, key);
        #endregion


        #region database

        #region 同步

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="key">主键</param>
        /// <param name="hashField"需要删除的字段</param>
        /// <returns></returns>
        public bool Delete(int dataBase, string key, string hashField)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return redisConnection.Data(dataBase).HashDelete(redisPrefixKey.HashPrefixKey + key, hashField);
        }

        /// <summary>
        /// 删除多条
        /// </summary>
        /// <param name="key">主键</param>
        /// <param name="hashFields"需要删除的字段</param>
        /// <returns></returns>
        public long HashDelete(int dataBase, string key, string[] hashFields)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (hashFields == null || hashFields.Count() <= 0)
                throw new ApplicationException("值不能为空");
            return redisConnection.Data(dataBase).HashDelete(redisPrefixKey.HashPrefixKey + key, hashFields.ToRedisValueArray());
        }

        /// <summary>
        /// 验证是否存在指定列
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public bool Exists(int dataBase, string key, string hashField)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return redisConnection.Data(dataBase).HashExists(redisPrefixKey.HashPrefixKey + key, hashField);
        }
        /// <summary>
        /// 获取指定的列的值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public string Get(int dataBase, string key, string hashField)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            var res = redisConnection.Data(dataBase).HashGet(redisPrefixKey.HashPrefixKey + key, hashField);
            return !res.IsNull ? res.ToString() : default;
        }
        /// <summary>
        /// 获取所有的数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetAll(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            var res = redisConnection.Data(dataBase).HashGetAll(redisPrefixKey.HashPrefixKey + key);
            return res != null ? res.ToStringDictionary() : default;
        }
        /// <summary>
        /// 获取多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashFields"></param>
        /// <returns></returns>
        public string[] Get(int dataBase, string key, string[] hashFields)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (hashFields == null || hashFields.Count() <= 0)
                throw new ApplicationException("值不能为空");
            var res = redisConnection.Data(dataBase).HashGet(redisPrefixKey.HashPrefixKey + key, hashFields.ToRedisValueArray());
            return res != null ? res.ToStringArray() : default;
        }
        /// <summary>
        /// 获取hash的长度
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long Length(int dataBase, string key) => redisConnection.Data(dataBase).HashLength(redisPrefixKey.HashPrefixKey + key);

        /// <summary>
        /// 存储hash值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashFields">存储的数据key-value结构</param>
        /// <returns></returns>
        public void Add(int dataBase, string key, HashEntry[] hashFields)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (hashFields == null || hashFields.Count() <= 0)
                throw new ApplicationException("值不能为空");
            redisConnection.Data(dataBase).HashSet(redisPrefixKey.HashPrefixKey + key, hashFields);
        }

        /// <summary>
        /// 储存单条hash值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField">字段名</param>
        /// <param name="value">值</param>
        /// <returns></returns>
        public bool Add(int dataBase, string key, string hashField, string value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return redisConnection.Data(dataBase).HashSet(redisPrefixKey.HashPrefixKey + key, hashField, value);
        }
        /// <summary>
        /// 返回所有值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string[] HashValues(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            var res = redisConnection.Data(dataBase).HashValues(redisPrefixKey.HashPrefixKey + key);
            return res != null ? res.ToStringArray() : default;
        }
        #endregion

        #region 异步

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="key">主键</param>
        /// <param name="hashField"需要删除的字段</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int dataBase, string key, string hashField)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return await redisConnection.Data(dataBase).HashDeleteAsync(redisPrefixKey.HashPrefixKey + key, hashField);
        }

        /// <summary>
        /// 删除多条
        /// </summary>
        /// <param name="key">主键</param>
        /// <param name="hashFields"需要删除的字段</param>
        /// <returns></returns>
        public async Task<long> DeleteAsync(int dataBase, string key, string[] hashFields)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (hashFields == null || hashFields.Count() <= 0)
                throw new ApplicationException("值不能为空");
            return await redisConnection.Data(dataBase).HashDeleteAsync(redisPrefixKey.HashPrefixKey + key, hashFields.ToRedisValueArray()).ConfigureAwait(false);
        }

        /// <summary>
        /// 验证是否存在指定列
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public async Task<bool> ExistsAsync(int dataBase, string key, string hashField)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return await redisConnection.Data(dataBase).HashExistsAsync(redisPrefixKey.HashPrefixKey + key, hashField).ConfigureAwait(false);
        }
        /// <summary>
        /// 获取指定的列的值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public async Task<string> GetAsync(int dataBase, string key, string hashField)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            var res = await redisConnection.Data(dataBase).HashGetAsync(redisPrefixKey.HashPrefixKey + key, hashField).ConfigureAwait(false);
            return !res.IsNull ? res.ToString() : default;
        }

        /// <summary>
        /// 获取所有的数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, string>> GetAllAsync(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            var res = await redisConnection.Data(dataBase).HashGetAllAsync(redisPrefixKey.HashPrefixKey + key).ConfigureAwait(false);
            return res != null ? res.ToStringDictionary() : default;
        }
        /// <summary>
        /// 获取多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashFields"></param>
        /// <returns></returns>
        public async Task<string[]> GetAsync(int dataBase, string key, string[] hashFields)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (hashFields == null || hashFields.Count() <= 0)
                throw new ApplicationException("值不能为空");
            var res = await redisConnection.Data(dataBase).HashGetAsync(redisPrefixKey.HashPrefixKey + key, hashFields.ToRedisValueArray()).ConfigureAwait(false);
            return res != null ? res.ToStringArray() : default;
        }
        /// <summary>
        /// 获取hash的长度
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<long> LengthAsync(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return await redisConnection.Data(dataBase).HashLengthAsync(redisPrefixKey.HashPrefixKey + key);
        }

        /// <summary>
        /// 存储hash值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashFields">存储的数据key-value结构</param>
        /// <returns></returns>
        public async Task AddAsync(int dataBase, string key, HashEntry[] hashFields)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (hashFields == null || hashFields.Count() <= 0)
                throw new ApplicationException("值不能为空");
            await redisConnection.Data(dataBase).HashSetAsync(redisPrefixKey.HashPrefixKey + key, hashFields);
        }

        /// <summary>
        /// 储存单条hash值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashField">字段名</param>
        /// <param name="value">值</param>
        /// <returns></returns>
        public async Task<bool> AddAsync(int dataBase, string key, string hashField, string value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return await redisConnection.Data(dataBase).HashSetAsync(redisPrefixKey.HashPrefixKey + key, hashField, value);
        }

        /// <summary>
        /// 返回所有值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<string[]> ValuesAsync(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            var res = await redisConnection.Data(dataBase).HashValuesAsync(redisPrefixKey.HashPrefixKey + key).ConfigureAwait(false);
            return res != null ? res.ToStringArray() : default;
        }
        #endregion

        #endregion
    }
}
