﻿using Naruto.Redis.Interface;
using Naruto.Redis.Config;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naruto.Redis.Connection;

namespace Naruto.Redis.Internal
{
    public class RedisList : IRedisList
    {

        private readonly IRedisConnection redisConnection;

        private readonly RedisPrefixKey redisPrefixKey;

        /// <summary>
        /// 实例化连接
        /// </summary>
        public RedisList(IRedisConnection _redisConnection, IOptions<RedisOptions> options)
        {
            redisConnection = _redisConnection;
            //初始化key的前缀
            redisPrefixKey = options.Value.RedisPrefix ?? new RedisPrefixKey();
        }
        #region 同步
        /// <summary>
        /// 存储list 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add<T>(string key, List<T> value) => Add<T>(redisConnection.DataBase, key, value);
        /// <summary>
        /// 取list 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public List<T> Get<T>(string key, long start = 0, long stop = -1) => Get<T>(redisConnection.DataBase, key, start, stop);
        /// <summary>
        /// 删除list集合的某一项
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value">value值</param>
        public long Remove<T>(string key, T value, long count = 0) => Remove<T>(redisConnection.DataBase, key, value, count);
        /// <summary>
        /// 删除并返回存储在key上的列表的第一个元素。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string LeftPop(string key) => LeftPop(redisConnection.DataBase, key);
        /// <summary>
        /// 往最后推送一个数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long RightPush(string key, string value) => RightPush(redisConnection.DataBase, key, value);
        /// <summary>
        /// 删除并返回存储在key上的列表的第一个元素。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T LeftPop<T>(string key) => LeftPop<T>(redisConnection.DataBase, key);
        /// <summary>
        /// 往最后推送一个数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long RightPush<T>(string key, T value) => RightPush<T>(redisConnection.DataBase, key, value);
        /// <summary>
        /// 往末尾推送多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long RightPush(string key, string[] value) => RightPush(redisConnection.DataBase, key, value);
        /// <summary>
        /// 往末尾推送多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long RightPush<T>(string key, List<T> value) => RightPush<T>(redisConnection.DataBase, key, value);
        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long Length(string key) => Length(redisConnection.DataBase, key);
        #endregion

        #region 异步
        /// <summary>
        /// 存储list 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public async Task AddAsync<T>(string key, List<T> value) => await AddAsync<T>(redisConnection.DataBase, key, value);
        /// <summary>
        /// 取list 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public async Task<List<T>> GetAsync<T>(string key, long start = 0, long stop = -1) => await GetAsync<T>(redisConnection.DataBase, key, start, stop);

        /// <summary>
        /// 取list 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public async Task<List<string>> GetAsync(string key, long start = 0, long stop = -1) => await GetAsync(redisConnection.DataBase, key, start, stop);

        /// <summary>
        /// 删除list集合的某一项
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value">value值</param>
        public async Task<long> RemoveAsync<T>(string key, T value, long count = 0) => await RemoveAsync<T>(redisConnection.DataBase, key, value, count);


        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<long> LengthAsync(string key) => await LengthAsync(redisConnection.DataBase, key);
        /// <summary>
        /// 删除并返回存储在key上的列表的第一个元素。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<string> LeftPopAsync(string key) => await LeftPopAsync(redisConnection.DataBase, key);
        /// <summary>
        /// 往最后推送一个数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<long> RightPushAsync(string key, string value) => await RightPushAsync(redisConnection.DataBase, key, value);
        /// <summary>
        /// 删除并返回存储在key上的列表的第一个元素。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<T> LeftPopAsync<T>(string key) => await LeftPopAsync<T>(redisConnection.DataBase, key);
        /// <summary>
        /// 往最后推送一个数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<long> RightPushAsync<T>(string key, T value) => await RightPushAsync<T>(redisConnection.DataBase, key, value);
        /// <summary>
        /// 往末尾推送多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<long> RightPushAsync<T>(string key, List<T> value) => await RightPushAsync<T>(redisConnection.DataBase, key, value);
        /// <summary>
        /// 往末尾推送多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<long> RightPushAsync(string key, string[] value) => await RightPushAsync(redisConnection.DataBase, key, value);
        #endregion

        #region database

        #region 同步
        /// <summary>
        /// 存储list 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add<T>(int dataBase, string key, List<T> value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value != null && value.Count > 0)
            {
                foreach (var single in value)
                {
                    var result = RedisBase.ConvertJson(single);
                    redisConnection.Data(dataBase).ListRightPush(redisPrefixKey.ListPrefixKey + key, result);
                }
            }
        }
        /// <summary>
        /// 取list 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public List<T> Get<T>(int dataBase, string key, long start = 0, long stop = -1)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            var vList = redisConnection.Data(dataBase).ListRange(redisPrefixKey.ListPrefixKey + key, start, stop);
            List<T> result = new List<T>();
            foreach (var item in vList)
            {
                var model = RedisBase.ConvertObj<T>(item); //反序列化
                result.Add(model);
            }
            return result;
        }
        /// <summary>
        /// 删除list集合的某一项
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value">value值</param>
        public long Remove<T>(int dataBase, string key, T value, long count = 0)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null)
                throw new ApplicationException("值不能为空");
            return redisConnection.Data(dataBase).ListRemove(redisPrefixKey.ListPrefixKey + key, RedisBase.ConvertJson(value), count);
        }
        /// <summary>
        /// 删除并返回存储在key上的列表的第一个元素。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string LeftPop(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return redisConnection.Data(dataBase).ListLeftPop(redisPrefixKey.ListPrefixKey + key);
        }
        /// <summary>
        /// 往最后推送一个数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long RightPush(int dataBase, string key, string value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null)
                throw new ApplicationException("值不能为空");
            return redisConnection.Data(dataBase).ListRightPush(redisPrefixKey.ListPrefixKey + key, value);
        }
        /// <summary>
        /// 删除并返回存储在key上的列表的第一个元素。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T LeftPop<T>(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return RedisBase.ConvertObj<T>(redisConnection.Data(dataBase).ListLeftPop(redisPrefixKey.ListPrefixKey + key));
        }
        /// <summary>
        /// 往最后推送一个数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long RightPush<T>(int dataBase, string key, T value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null)
                throw new ApplicationException("值不能为空");
            return redisConnection.Data(dataBase).ListRightPush(redisPrefixKey.ListPrefixKey + key, RedisBase.ConvertJson(value));
        }
        /// <summary>
        /// 往末尾推送多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long RightPush(int dataBase, string key, string[] value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null || value.Count() <= 0)
                throw new ApplicationException("值不能为空");
            return redisConnection.Data(dataBase).ListRightPush(redisPrefixKey.ListPrefixKey + key, value.ToRedisValueArray());
        }
        /// <summary>
        /// 往末尾推送多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public long RightPush<T>(int dataBase, string key, List<T> value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null || value.Count <= 0)
                throw new ApplicationException("值不能为空");
            RedisValue[] redisValues = new RedisValue[value.Count];
            for (int i = 0; i < value.Count; i++)
            {
                redisValues[i] = RedisBase.ConvertJson(value[i]);
            }
            return redisConnection.Data(dataBase).ListRightPush(redisPrefixKey.ListPrefixKey + key, redisValues);
        }
        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long Length(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return redisConnection.Data(dataBase).ListLength(redisPrefixKey.ListPrefixKey + key);
        }
        #endregion

        #region 异步
        /// <summary>
        /// 存储list 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public async Task AddAsync<T>(int dataBase, string key, List<T> value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value != null && value.Count > 0)
            {
                foreach (var single in value)
                {
                    var result = RedisBase.ConvertJson(single);
                    await redisConnection.Data(dataBase).ListRightPushAsync(redisPrefixKey.ListPrefixKey + key, result).ConfigureAwait(false);
                }
            }
        }
        /// <summary>
        /// 取list 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public async Task<List<T>> GetAsync<T>(int dataBase, string key, long start = 0, long stop = -1)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            var vList = await redisConnection.Data(dataBase).ListRangeAsync(redisPrefixKey.ListPrefixKey + key, start, stop).ConfigureAwait(false);
            List<T> result = new List<T>();
            foreach (var item in vList)
            {
                var model = RedisBase.ConvertObj<T>(item); //反序列化
                result.Add(model);
            }
            return result;
        }

        /// <summary>
        /// 取list 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public async Task<List<string>> GetAsync(int dataBase, string key, long start = 0, long stop = -1)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            var vList = await redisConnection.Data(dataBase).ListRangeAsync(redisPrefixKey.ListPrefixKey + key, start, stop).ConfigureAwait(false);
            return vList.ToStringArray().ToList();
        }

        /// <summary>
        /// 删除list集合的某一项
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value">value值</param>
        public async Task<long> RemoveAsync<T>(int dataBase, string key, T value, long count = 0)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null)
                throw new ApplicationException("值不能为空");
            return await redisConnection.Data(dataBase).ListRemoveAsync(redisPrefixKey.ListPrefixKey + key, RedisBase.ConvertJson(value), count).ConfigureAwait(false);
        }


        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<long> LengthAsync(int dataBase, string key)
        {
            return await redisConnection.Data(dataBase).ListLengthAsync(redisPrefixKey.ListPrefixKey + key).ConfigureAwait(false);
        }

        /// <summary>
        /// 删除并返回存储在key上的列表的第一个元素。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<string> LeftPopAsync(int dataBase, string key)
        {
            return await redisConnection.Data(dataBase).ListLeftPopAsync(redisPrefixKey.ListPrefixKey + key).ConfigureAwait(false);
        }
        /// <summary>
        /// 往最后推送一个数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<long> RightPushAsync(int dataBase, string key, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ApplicationException("值不能为空");
            return await redisConnection.Data(dataBase).ListRightPushAsync(redisPrefixKey.ListPrefixKey + key, value).ConfigureAwait(false);
        }
        /// <summary>
        /// 删除并返回存储在key上的列表的第一个元素。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<T> LeftPopAsync<T>(int dataBase, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return RedisBase.ConvertObj<T>((await redisConnection.Data(dataBase).ListLeftPopAsync(redisPrefixKey.ListPrefixKey + key).ConfigureAwait(false)));
        }
        /// <summary>
        /// 往最后推送一个数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<long> RightPushAsync<T>(int dataBase, string key, T value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null)
                throw new ApplicationException("值不能为空");
            return await redisConnection.Data(dataBase).ListRightPushAsync(redisPrefixKey.ListPrefixKey + key, RedisBase.ConvertJson(value)).ConfigureAwait(false);
        }
        /// <summary>
        /// 往末尾推送多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<long> RightPushAsync<T>(int dataBase, string key, List<T> value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null || value.Count <= 0)
                throw new ApplicationException("值不能为空");
            List<RedisValue> redisValues = new List<RedisValue>();
            value.ForEach(item =>
            {
                redisValues.Add(RedisBase.ConvertJson(item));
            });
            return await redisConnection.Data(dataBase).ListRightPushAsync(redisPrefixKey.ListPrefixKey + key, redisValues.ToArray()).ConfigureAwait(false);
        }
        /// <summary>
        /// 往末尾推送多条数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<long> RightPushAsync(int dataBase, string key, string[] value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null || value.Count() <= 0)
                throw new ApplicationException("值不能为空");
            return await redisConnection.Data(dataBase).ListRightPushAsync(redisPrefixKey.ListPrefixKey + key, value.ToRedisValueArray()).ConfigureAwait(false);
        }
        #endregion

        #endregion
    }
}
