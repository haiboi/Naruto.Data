﻿using Naruto.Redis.Interface;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Redis.Connection
{
    /// <summary>
    /// redis的连接配置
    /// </summary>
    public interface IRedisConnection
    {
        /// <summary>
        /// redis 密码
        /// </summary>
        string Password { get; }
        /// <summary>
        /// redis的默认存储库
        /// </summary>

        int DataBase { get; }
        /// <summary>
        /// redis 连接
        /// </summary>

        string[] ConnectionEndPoint { get; }
        /// <summary>
        /// 默认超时时间
        /// </summary>
        int ConnectTimeout { get; }
        /// <summary>
        /// 默认异步超时时间
        /// </summary>
        int AsyncTimeout { get; }
        /// <summary>
        /// 连接实例
        /// </summary>
        ConnectionMultiplexer Connection { get; }

        /// <summary>
        /// 
        /// </summary>
        IDatabase Data(int dataBase);
    }
}
