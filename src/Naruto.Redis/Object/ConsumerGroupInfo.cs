﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Redis.Object
{
    /// <summary>
    /// 消费者组信息
    /// </summary>
    public class ConsumerGroupInfo
    {

        /// <summary>
        /// 消费者组名称
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        //
        // 摘要:
        //     The number of consumers within the consumer group.
        public int ConsumerCount
        {
            get;
            set;
        }

        //
        // 摘要:
        //     The total number of pending messages for the consumer group. A pending message
        //     is one that has been received by a consumer but not yet acknowledged.
        public int PendingMessageCount
        {
            get;
            set;
        }

        //
        // 摘要:
        //     The Id of the last message delivered to the group
        public string LastDeliveredId
        {
            get;
            set;
        }
    }
}
