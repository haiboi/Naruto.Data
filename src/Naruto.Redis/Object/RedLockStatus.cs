﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Redis.Object
{
    /// <summary>
    /// 
    /// </summary>
    public enum RedLockStatus
    {
        /// <summary>
        /// 获取到锁
        /// </summary>
        Acquired,
        /// <summary>
        /// 没有获取到锁
        /// </summary>
        UnLock,
        /// <summary>
        /// 报错
        /// </summary>
        Error
    }
}
